

SELECT
  beer_style                 beer_style,
  AVG(abv)                   avg_abv,
  AVG(ibu)                   avg_ibu,
  AVG(ounces)                avg_ounces
FROM
  {{ ref('beers') }}
GROUP BY
  beer_style
